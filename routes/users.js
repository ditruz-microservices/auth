const express = require('express');
const router = express.Router();
const argon2 = require('argon2');
const jwt = require('jsonwebtoken');
// const { QueryTypes } = require('sequelize');

const model = require('../models/index');

const passportJWT = require('../middlewares/passport-jwt');

// get profile
/* localhost:3000/api/v1/users/profile */
router.get('/profile', [ passportJWT.isLogin ] ,async function(req, res, next) {
  const user = await model.User.findByPk(req.user.user_id);

  return res.status(200).json({
    user: {
      id: user.id,
      fullname: user.fullname,
      email: user.email,
      created_at: user.created_at
    }
  });
  
});

/* localhost:3000/api/v1/users/ */
router.get('/', async function(req, res, next) {

  // const users = await model.sequelize.query("SELECT * FROM `users`", { type: QueryTypes.SELECT });

  // const users = await model.User.findAll();
   const users = await model.User.findAll({
     // attributes: ['id','email'],
     attributes: { exclude: ['password'] },
     order: [['id','desc']]
   });

  const totalRecord = await model.User.count();

  return res.status(200).json({
    total: totalRecord,
    data: users
  });
});

/* localhost:3000/api/v1/users/register */
router.post('/register', async function(req, res, next) {
  const { fullname, email, password } = req.body;

  //1. เช็คอีเมล์ซ้ำ
  const user = await model.User.findOne({ where: { email: email } });
  if (user !== null) {
    return res.status(409).json({ message: 'อีเมล์ซ้ำ มีผู้ใช้งานแล้ว' });
  }

  //2. เข้ารหัส password
  const passwordHash = await argon2.hash(password);

  //3. เพิ่มข้อมูลไปที่ตาราง users
  const newUser = await model.User.create({
    fullname: fullname,
    email: email,
    password: passwordHash
  });

  return res.status(201).json({
    message: 'ลงทะเบียนสำเร็จ ',
    user: {
      id: newUser.id,
      fullname: newUser.fullname,
      email: newUser.email,
      created_at: newUser.created_at
    }
  });
});

/* localhost:3000/api/v1/users/login */
router.post('/login', async function(req, res, next) {

  const { email, password } = req.body;

  //1. นำ email ที่ส่งมาไปเช็คว่ามีในระบบหรือไม่
  const user = await model.User.findOne({ where: { email: email } });
  if (user === null) {
    return res.status(404).json({ message: 'ไม่พบอีเมล์นี้ในระบบ' });
  }

  //2. นำ password จาก client มาเปรียบเทียบกับ password จากตาราง users
  const isValid = await argon2.verify(user.password, password);
  if (isValid === false) {
    return res.status(401).json({ message: 'รหัสผ่านไม่ถูกต้อง' });
  }

  //3. สร้าง token
  const token = jwt.sign({ user_id: user.id }, process.env.JWT_KEY, { expiresIn: "7d" });

  return res.status(200).json({
    message: 'เข้าระบบสำเร็จ',
    access_token: token
  });

});


module.exports = router;
